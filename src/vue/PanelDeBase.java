package vue;

import java.awt.Color;

import javax.swing.JPanel;

public abstract class PanelDeBase extends JPanel
{
	public PanelDeBase(Color uneCouleur)
	{
		this.setBounds(40, 40, 720, 320);
		this.setBackground(uneCouleur);
		this.setLayout(null);
		this.setVisible(false);
	}
}