package vue;

import java.awt.Color;
import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import controleur.SondageNum;
import controleur.Sondeur;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class VueGenerale extends JFrame implements ActionListener
{
	
	private JPanel panelMenu = new JPanel();
	private JButton btProfil = new JButton("Profil");
	private JButton btCategories = new JButton("Categories");
	private JButton btSousCategories = new JButton("Sous-Categories");
	private JButton btSondages = new JButton("Sondages");
	private JButton btStats= new JButton("Statistiques");
	private JButton btBord = new JButton("T-Bord");
	private JButton btDeconnexion = new JButton("Déconnexion");

	private JPanel panelProfil = new JPanel();
	private PanelCategorie unPanelCategorie = new PanelCategorie();
	private PanelStats unPanelStats = new PanelStats();
	
	public VueGenerale(Sondeur unUser)
	{
		
		this.setTitle("Administration Sondage-Num");
		this.setBounds(200, 100, 850, 400);
		this.getContentPane().setBackground(Color.blue);
		this.setResizable(true);
		this.setLayout(null);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		//Construction du Panel Menu
		this.panelMenu.setLayout(new GridLayout(1, 7));
		this.panelMenu.setBounds(20, 20, 760, 40);
		this.panelMenu.add(this.btProfil);
		this.panelMenu.add(this.btCategories);
		this.panelMenu.add(this.btSousCategories);
		this.panelMenu.add(this.btSondages);
		this.panelMenu.add(this.btStats);
		this.panelMenu.add(this.btBord);
		this.panelMenu.add(this.btDeconnexion);
		this.panelMenu.setBackground(Color.blue);
		this.add(this.panelMenu);
		
		//Construction du Panel Profil
		this.panelProfil.setBounds(100, 100, 500, 200);
		this.panelProfil.setLayout(new GridLayout(4, 1));
		this.panelProfil.add(new JLabel("Nom User : " + unUser.getNom()));
		this.panelProfil.add(new JLabel("Email User : " + unUser.getEmail()));
		this.panelProfil.add(new JLabel("Telephone User : " + unUser.getTel()));
		this.panelProfil.add(new JLabel("Adresse User : " + unUser.getAdresse()));
		this.panelProfil.add(new JLabel("Role User : " + unUser.getRole()));
		this.panelProfil.setVisible(false);
		this.add(this.panelProfil);
		
		//ajouts des panels dans la fenetre 
		this.add(this.unPanelCategorie);
		this.add(this.unPanelStats);
		
		//Rendre les boutons ecoutable
		this.btDeconnexion.addActionListener(this);
		this.btProfil.addActionListener(this);
		this.btCategories.addActionListener(this);
		this.btSousCategories.addActionListener(this);
		this.btSondages.addActionListener(this);
		this.btStats.addActionListener(this);
		this.btBord.addActionListener(this);
		
		this.setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == this.btDeconnexion)
		{
			SondageNum.detruireVueGenerale(); // Quand je quitte l'appli generele, je détruis la vue puis je re affiche la vue connexion
			SondageNum.rendreVisibleVueConnexion(true);
			
		}
		else if(e.getSource() == this.btProfil)
		{
			this.panelProfil.setVisible(true);
			this.unPanelCategorie.setVisible(false);
			this.unPanelStats.setVisible(false);
		}
		else if (e.getSource() == this.btCategories)
		{
			this.unPanelCategorie.setVisible(true);
			this.panelProfil.setVisible(false);
			this.unPanelStats.setVisible(false);
		}
		else if (e.getSource() == this.btStats)
		{
			this.unPanelStats.setVisible(true);
			this.unPanelCategorie.setVisible(false);
			this.panelProfil.setVisible(false);
		}
		
	}
}
