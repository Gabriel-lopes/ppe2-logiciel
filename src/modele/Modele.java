package modele;

import java.sql.Statement;
import java.util.ArrayList;
import java.sql.ResultSet;
import java.sql.SQLException;

import controleur.Sondeur;
import controleur.Categorie;
import controleur.Sous_categorie;
import controleur.TableauBord;
import controleur.Sondage;


public class Modele {
	private static Bdd uneBdd = new Bdd("localhost", "sondagedatabase", "root", "");
	
	
	// --------------------------------------- Gestion User --------------------------------------- //
	
	public static Sondeur selectWhereUser (String email, String mdp)
	{
		String requete ="select * from sondeur where email ='"+email+"' and mdp ='" + mdp +"';";
		Sondeur unUser = null; 
		try {
			uneBdd.seConnecter();
			Statement unStat = uneBdd.getMaConnexion().createStatement();
			ResultSet unResultat = unStat.executeQuery(requete); 
			if(unResultat.next())
			{
				unUser = new Sondeur (
						unResultat.getInt("idSondeur"), 
						unResultat.getString("nom"), 
						unResultat.getString("email"),
						unResultat.getString("tel"), 
						unResultat.getString("adresse"), 
						unResultat.getString("mdp"), 
						unResultat.getString("role")
						);
			}
			unStat.close();
			uneBdd.seDeConnecter();
		}
		catch (SQLException exp)
		{
			System.out.println("Erreur d'execution de la requete :" + requete);	
		}
		return unUser; 
	}
	
	
	// --------------------------------------- Gestion Sondeur --------------------------------------- //
	
	public static Sondeur SelectWhereSondeur(String email, String mdp)
	{
		String requete = "select * from sondeur where email='" + email + "' and mdp='" + mdp + "'; ";
		Sondeur unSondeur = null;
		try
		{
			uneBdd.seConnecter();
			Statement unStat = uneBdd.getMaConnexion().createStatement();
			ResultSet unResultat = unStat.executeQuery(requete);
			if(unResultat.next())
			{
				unSondeur = new Sondeur(
						unResultat.getInt("idSondeur"),
						unResultat.getString("nom"),
						unResultat.getString("email"),
						unResultat.getString("tel"),
						unResultat.getString("adresse"),
						unResultat.getString("mdp"),
						unResultat.getString("role")
						);

			}
			unStat.close();
			uneBdd.seDeConnecter();
		}
		catch(SQLException exp)
		{
			System.out.println("Erreur d'execution de la requete : " + requete);
		}
		
		return unSondeur;
	}
	
	// --------------------------------------- Categorie --------------------------------------- //
	
	public static void insertCategorie(Categorie uneCategorie)
	{
		String requete = "insert into categorie values(null, '" + uneCategorie.getNom()
		+ "','" + uneCategorie.getDescription() + "'); ";	
		try 
		{
			uneBdd.seConnecter();
			Statement unStat = uneBdd.getMaConnexion().createStatement(); //curseur d'execusion 
			unStat.execute(requete); // execute comme php
			unStat.close();
			uneBdd.seDeConnecter();
		}
		catch(SQLException exp)
		{
			System.out.println("Erreur d'execution de la requete : " + requete);
		}
	}
	
	public static void UpdateCategorie(Categorie uneCategorie)
	{
		String requete = "update categorie set nom = '" + uneCategorie.getNom()
		+ "', description= '" + uneCategorie.getDescription() + "'; ";	
		try
		{
			uneBdd.seConnecter();
			Statement unStat = uneBdd.getMaConnexion().createStatement(); //curseur d'execusion 
			unStat.execute(requete); // execute comme php
			unStat.close();
			uneBdd.seDeConnecter();
		}
		catch(SQLException exp)
		{
			System.out.println("Erreur d'execution de la requete : " + requete);
		}
	}
	
	public static ArrayList<Categorie> selectAllCategories(String mot)
	{
		String requete = ""; 
		if (mot.equals("")){
			requete = "select * from categorie ;";
		}else {
			requete = " select * from categorie where nom like '%"+mot+"%' or description='%"+mot+"%'"; 
		}
		ArrayList<Categorie> lesCategories = new ArrayList<Categorie>();
		try
		{
			uneBdd.seConnecter();
			Statement unStat = uneBdd.getMaConnexion().createStatement();
			
			//FetchAll en php : extraction des donn�es
			
			ResultSet desResultats = unStat.executeQuery(requete);
			//Tant qu'il y a des r�sultats
			
			while(desResultats.next())
			{
				Categorie uneCategorie = new Categorie(
						desResultats.getInt("idCategorie"),
						desResultats.getString("nom"),
						desResultats.getString("description")
						);
				
				//Ajouter l'instance pilote dans l'ArrayList
				lesCategories.add(uneCategorie);
			
			}
			unStat.close();
			uneBdd.seDeConnecter();
		}
		catch(SQLException exp)
		{
			System.out.println("Erreur d'execution de la requete : " + requete);
		}
		return lesCategories;
	}
	
	public static Categorie selectWhereCategorie(int idCategorie)
	{
		String requete = "select * from categorie where idCategorie = + " + idCategorie;
		Categorie uneCategorie = null; // � r�cupere de la base de donn�es
		
		try
		{
			uneBdd.seConnecter();
			Statement unStat = uneBdd.getMaConnexion().createStatement();
			
			//FetchAll en php : extraction des donn�es
			ResultSet unResultat = unStat.executeQuery(requete);
			
			//s'il y a un r�sultat
			if(unResultat.next())
			{
				uneCategorie = new Categorie(
						unResultat.getInt("idCategorie"),
						unResultat.getString("nom"),
						unResultat.getString("description")
						);

			}
			unStat.close();
			uneBdd.seDeConnecter();
		}
		catch(SQLException exp)
		{
			System.out.println("Erreur d'execution de la requete : " + requete);
		}
		return uneCategorie;
	}
	
	//Surchargeur
	public static Categorie selectWhereCategorie (String nom, String description)
	{
		String requete = "select * from categorie where nom = '" + nom +"' and description ='" + description +"';"; 
		Categorie uneCategorie = null ; 
		try {
			uneBdd.seConnecter();
			Statement unStat = uneBdd.getMaConnexion().createStatement();
			//fetchAll en PHP : extraction des données 
			ResultSet unResultat = unStat.executeQuery(requete); 
			//s'il y a un résultat : un seul Pilote
			if(unResultat.next())
			{
				uneCategorie = new Categorie (
						unResultat.getInt("idCategorie"), 
						unResultat.getString("nom"), 
						unResultat.getString("description")
						);
			}
			unStat.close();
			uneBdd.seDeConnecter();
		}
		catch (SQLException exp)
		{
			System.out.println("Erreur d'execution de la requete :" + requete);	
		}
		return uneCategorie;
	}
	
	public static void supprimerCategorie(int idCategorie) {
		String requete = "delete from categorie where idCategorie = " + idCategorie;
		try {
			uneBdd.seConnecter();
			Statement unStat = uneBdd.getMaConnexion().createStatement();
			unStat.execute(requete);
			unStat.close();
			uneBdd.seDeConnecter();
		}
		catch(SQLException exp) {
			System.out.println("Erreur d'execution de la requete " + requete);
		}
	}
	
	public static int countCategorie()
	{
		int nb = 0;
		String requete = "select count(idCategorie) as nb from categorie ;";
		try
		{
			uneBdd.seConnecter();
			Statement unStat = uneBdd.getMaConnexion().createStatement();
			ResultSet unResultat  = unStat.executeQuery(requete);
			if(unResultat.next())
			{
				nb = unResultat.getInt("nb");
			}
			unStat.close();
			uneBdd.seDeConnecter();
		}
		catch(SQLException exp)
		{
			System.out.println("Erreur d'execution de la requete : " + requete);
		}
		return nb;
	}
	
	public static void updateCategorie(Categorie uneCategorie)
	{
		String requete = "update categorie set nom = '" + uneCategorie.getNom()
		+ "', description = '" + uneCategorie.getDescription()  + "' where idCategorie = " + uneCategorie.getIdCategorie() + " ; ";	
		try
		{
			uneBdd.seConnecter();
			Statement unStat = uneBdd.getMaConnexion().createStatement(); //curseur d'execusion 
			unStat.execute(requete); // execute comme php
			unStat.close();
			uneBdd.seDeConnecter();
		}
		catch(SQLException exp)
		{
			System.out.println("Erreur d'execution de la requete : " + requete);
		}	
	}
	
	
	// --------------------------------------- Sous_Categorie --------------------------------------- //
	
		public static void insertSousCategorie(Sous_categorie uneSousCategorie)
		{
			String requete = "insert into sous_categorie values(null, '" + uneSousCategorie.getNom()
			+ "','" + uneSousCategorie.getDescription() + "','" + uneSousCategorie.getIdCategorie() +  "'); ";	
			try 
			{
				uneBdd.seConnecter();
				Statement unStat = uneBdd.getMaConnexion().createStatement(); //curseur d'execusion 
				unStat.execute(requete); // execute comme php
				unStat.close();
				uneBdd.seDeConnecter();
			}
			catch(SQLException exp)
			{
				System.out.println("Erreur d'execution de la requete : " + requete);
			}
		}
		
		public static void UpdateSousCategorie(Sous_categorie uneSousCategorie)
		{
			String requete = "update categorie set nom = '" + uneSousCategorie.getNom()
			+ "', description= '" + uneSousCategorie.getDescription() + "', idCategorie='" + uneSousCategorie.getIdCategorie() + "'; ";	
			try
			{
				uneBdd.seConnecter();
				Statement unStat = uneBdd.getMaConnexion().createStatement(); //curseur d'execusion 
				unStat.execute(requete); // execute comme php
				unStat.close();
				uneBdd.seDeConnecter();
			}
			catch(SQLException exp)
			{
				System.out.println("Erreur d'execution de la requete : " + requete);
			}
		}
		
		public static ArrayList<Sous_categorie> selectAllSousCategories()
		{
			String requete = "select * from Sous_categorie ;";
			ArrayList<Sous_categorie> lesSous_categories = new ArrayList<Sous_categorie>();
			try
			{
				uneBdd.seConnecter();
				Statement unStat = uneBdd.getMaConnexion().createStatement();
				
				//FetchAll en php : extraction des donn�es
				
				ResultSet desResultats = unStat.executeQuery(requete);
				//Tant qu'il y a des r�sultats
				
				while(desResultats.next())
				{
					Sous_categorie uneSous_categorie = new Sous_categorie(
							desResultats.getInt("idSous_categorie"),
							desResultats.getInt("idCategorie"),
							desResultats.getString("nom"),
							desResultats.getString("description")
							);
					
					//Ajouter l'instance pilote dans l'ArrayList
					lesSous_categories.add(uneSous_categorie);
				
				}
				unStat.close();
				uneBdd.seDeConnecter();
			}
			catch(SQLException exp)
			{
				System.out.println("Erreur d'execution de la requete : " + requete);
			}
			return lesSous_categories;
		}
		
		public static Sous_categorie selectWhereSous_categorie(int idSous_categorie)
		{
			String requete = "select * from Sous_categorie where idSous_categorie = + " + idSous_categorie;
			Sous_categorie uneSous_categorie = null; // � r�cupere de la base de donn�es
			
			try
			{
				uneBdd.seConnecter();
				Statement unStat = uneBdd.getMaConnexion().createStatement();
				
				//FetchAll en php : extraction des donn�es
				ResultSet unResultat = unStat.executeQuery(requete);
				
				//s'il y a un r�sultat
				if(unResultat.next())
				{
					uneSous_categorie = new Sous_categorie(
							unResultat.getInt("idSous_categorie"),
							unResultat.getInt("idCategorie"),
							unResultat.getString("nom"),
							unResultat.getString("description")
							);

				}
				unStat.close();
				uneBdd.seDeConnecter();
			}
			catch(SQLException exp)
			{
				System.out.println("Erreur d'execution de la requete : " + requete);
			}
			return uneSous_categorie;
		}
		
		public static void deleteSous_categorie(int idSous_categorie)
		{
			String requete = "delete from sous_categorie where idSous_categorie = " + idSous_categorie;
			
			try
			{
				uneBdd.seConnecter();
				Statement unStat = uneBdd.getMaConnexion().createStatement(); //curseur d'execusion 
				unStat.execute(requete); // execute comme php
				unStat.close();
				uneBdd.seDeConnecter();
			}
			catch(SQLException exp)
			{
				System.out.println("Erreur d'execution de la requete : " + requete);
			}
		}
		
		public static int countSous_categorie()
		{
			int nb = 0;
			String requete = "select count(idSous_categorie) as nb from sous_categorie ;";
			try
			{
				uneBdd.seConnecter();
				Statement unStat = uneBdd.getMaConnexion().createStatement();
				ResultSet unResultat  = unStat.executeQuery(requete);
				if(unResultat.next())
				{
					nb = unResultat.getInt("nb");
				}
				unStat.close();
				uneBdd.seDeConnecter();
			}
			catch(SQLException exp)
			{
				System.out.println("Erreur d'execution de la requete : " + requete);
			}
			return nb;
		}
	
	// --------------------------------------- Sondage --------------------------------------- //
	
	public static void insertSondage(Sondage unSondage)
	{
		String requete = "insert into sondage values(null, '" + unSondage.getLibelle()
		+ "','" + unSondage.getDateCreation() + "','" + unSondage.getEtat() + "','" + unSondage.getIdSondeur() + "','" + unSondage.getIdSous_categorie() + "'); ";	
		
		try
		{
			uneBdd.seConnecter();
			Statement unStat = uneBdd.getMaConnexion().createStatement(); //curseur d'execusion 
			unStat.execute(requete); // execute comme php
			unStat.close();
			uneBdd.seDeConnecter();
		}
		catch(SQLException exp)
		{
			System.out.println("Erreur d'execution de la requete : " + requete);
		}
	}
	
	public static void updateSondage(Sondage unSondage)
	{
		String requete = "update sondage set libelle= '" + unSondage.getLibelle()
		+ "', dateCreation= '" + unSondage.getDateCreation() + "',etat= '" + unSondage.getEtat() + "',idSondeur= '" + unSondage.getIdSondeur() + "',idSous_categorie=  '" +
		unSondage.getIdSous_categorie() + "; ";
		
		try
		{
			uneBdd.seConnecter();
			Statement unStat = uneBdd.getMaConnexion().createStatement(); //curseur d'execusion 
			unStat.execute(requete); // execute comme php
			unStat.close();
			uneBdd.seDeConnecter();
		}
		catch(SQLException exp)
		{
			System.out.println("Erreur d'execution de la requete : " + requete);
		}
	}
	
	public static ArrayList<Sondage> selectAllSondage ()

	{
		String requete = "select * from sondage ; "; 
		ArrayList<Sondage> lesSondages = new ArrayList<Sondage>(); 
		try {
			uneBdd.seConnecter();
			Statement unStat = uneBdd.getMaConnexion().createStatement();
			//fetchAll en PHP : extraction des donn�es 
			ResultSet desResultats = unStat.executeQuery(requete); 
			//tant qu'il y a des r�sultats 
			while(desResultats.next())
			{
				Sondage unSondage = new Sondage (
						desResultats.getInt("idSondage"), 
						desResultats.getInt("idSondeur"),
						desResultats.getInt("idSous_categorie"),
						desResultats.getDate("dateCreation"), 
						desResultats.getString("libelle"), 
						desResultats.getString("etat")
						);
				//ajouter l'instance Vol dans l'ArrayList
				lesSondages.add(unSondage);
			}
			
			unStat.close();
			uneBdd.seDeConnecter();
		}
		catch (SQLException exp)
		{
			System.out.println("Erreur d'execution de la requete :" + requete);	
		}
		return lesSondages;
	}
	
	public static Sondage selectWhereSondage(int idSondage)
	{
		String requete = "select * from sondage where idSondage =" + idSondage;
		Sondage unSondage = null;
		
		try {
			uneBdd.seConnecter();
			Statement unStat = uneBdd.getMaConnexion().createStatement();
			//fetchAll en PHP : extraction des donn�es 
			ResultSet unResultat = unStat.executeQuery(requete);
			
			//S'il y a un r�sultat... 
			if(unResultat.next())
			{
				unSondage = new Sondage (
						unResultat.getInt("idSondage"), 
						unResultat.getInt("idSondeur"), 
						unResultat.getInt("idSous_categorie"), 
						unResultat.getDate("dateCreation"), 
						unResultat.getString("libelle"), 
						unResultat.getString("etat") 
						);
			}
			
			unStat.close();
			uneBdd.seDeConnecter();
		}
		catch (SQLException exp)
		{
			System.out.println("Erreur d'execution de la requete :" + requete);	
		}
		return unSondage;
	}
	
	public static void deleteSondage(int idSondage)
	{
		String requete = "delete from Sondage where idSondage = " + idSondage;
		
		try
		{
			uneBdd.seConnecter();
			Statement unStat = uneBdd.getMaConnexion().createStatement(); //curseur d'execusion 
			unStat.execute(requete); // execute comme php
			unStat.close();
			uneBdd.seDeConnecter();
		}
		catch(SQLException exp)
		{
			System.out.println("Erreur d'execution de la requete : " + requete);
		}
	}

	public static int countSondage()
	{
		int nb = 0;
		String requete = "select count(idSondage) as nb from sondage ;";
		try
		{
			uneBdd.seConnecter();
			Statement unStat = uneBdd.getMaConnexion().createStatement();
			ResultSet unResultat  = unStat.executeQuery(requete);
			if(unResultat.next())
			{
				nb = unResultat.getInt("nb");
			}
			unStat.close();
			uneBdd.seDeConnecter();
		}
		catch(SQLException exp)
		{
			System.out.println("Erreur d'execution de la requete : " + requete);
		}
		return nb;
	}
	
	// --------------------------------------- Tableau de bord  --------------------------------------- //
	
	public static ArrayList<TableauBord> selectAllTableauxBords(String mot) {
		String requete = "";
		if(mot == "") {
			requete = "select * from categorie; ";
		} else {
			requete = "select * from categorie where nom like '%"+ mot + "%' or description like '" + mot + "%'";
		}
		ArrayList<TableauBord> lesTableauBords = new ArrayList<TableauBord>();
		try {
			uneBdd.seConnecter();
			Statement unStat = uneBdd.getMaConnexion().createStatement(); //curseur d'execusion 
			//fetchAll en php : extraction des données
			ResultSet desResultats = unStat.executeQuery(requete);
			//tant qu'il y a des résultats :
			while(desResultats.next()) {
				TableauBord unTableauBord = new TableauBord (
					desResultats.getString("nom"),
					desResultats.getString("description")
				);
				//Ajouter l'instance pilote dans l'ArrayList
				lesTableauBords.add(unTableauBord);
			}
			unStat.close();
		}
		catch(SQLException exp) {
			System.out.println("Erreur d'execution de la requete : " + requete);
		}
		return lesTableauBords;
	}
}
