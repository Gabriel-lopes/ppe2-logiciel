package controleur;

public class Question {
	private int idQuestion, idSondage;
	private String question;
	
	public  Question(int idQuestion, int idSondage, String question) {

		this.idQuestion = idQuestion;
		this.idSondage = idSondage;
		this.question = question;
	}
	public  Question(int idSondage, String question) {

		this.idQuestion = 0;
		this.idSondage = idSondage;
		this.question = question;
	}
	public int getIdQuestion() {
		return idQuestion;
	}
	public void setIdQuestion(int idQuestion) {
		this.idQuestion = idQuestion;
	}
	public int getIdSondage() {
		return idSondage;
	}
	public void setIdSondage(int idSondage) {
		this.idSondage = idSondage;
	}
	public String getQuestion() {
		return question;
	}
	public void setQuestion(String question) {
		this.question = question;
	}
	
	
}
