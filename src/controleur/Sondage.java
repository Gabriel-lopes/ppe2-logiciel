package controleur;

import java.sql.Date;

public class Sondage {
	private int idSondage, idSondeur, idSous_categorie;
	private Date dateCreation;
	private String libelle;
	private String etat; //'Ouvert' ou 'Ferme'
	
	public Sondage(int idSondage, int idSondeur, int idSous_categorie, Date dateCreation, String libelle, String etat) {
		
		this.idSondage = idSondage;
		this.idSondeur = idSondeur;
		this.idSous_categorie = idSous_categorie;
		this.dateCreation = dateCreation;
		this.libelle = libelle;
		this.etat = etat;
	}
	
	public Sondage(int idSondeur, int idSous_categorie, Date dateCreation, String libelle, String etat) {
		
		this.idSondage = 0;
		this.idSondeur = idSondeur;
		this.idSous_categorie = idSous_categorie;
		this.dateCreation = dateCreation;
		this.libelle = libelle;
		this.etat = etat;
	}

	public int getIdSondage() {
		return idSondage;
	}

	public void setIdSondage(int idSondage) {
		this.idSondage = idSondage;
	}

	public int getIdSondeur() {
		return idSondeur;
	}

	public void setIdSondeur(int idSondeur) {
		this.idSondeur = idSondeur;
	}

	public int getIdSous_categorie() {
		return idSous_categorie;
	}

	public void setIdSous_categorie(int idSous_categorie) {
		this.idSous_categorie = idSous_categorie;
	}

	public Date getDateCreation() {
		return dateCreation;
	}

	public void setDateCreation(Date dateCreation) {
		this.dateCreation = dateCreation;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public String getEtat() {
		return etat;
	}

	public void setEtat(String etat) {
		this.etat = etat;
	}
	
	
}
