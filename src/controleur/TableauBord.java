package controleur;

public class TableauBord {
	private String nom, description; 
	//private String avion, vol, date;
	
	public TableauBord(String nom, String description) {
		this.nom = nom;
		this.description = description;
		/*
		this.avion = avion;
		this.vol = vol;
		this.date = date;
		*/
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
}