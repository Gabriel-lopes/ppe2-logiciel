package controleur;

import modele.Modele;
import vue.VueConnexion;
import vue.VueGenerale;

public class SondageNum {

	private static VueConnexion uneVueConnexion ; 
	private static VueGenerale uneVueGenerale ; 
	
	public static void instancierVueGenerale (Sondeur unUser)
	{
		uneVueGenerale = new VueGenerale(unUser); 
	}
	public static void detruireVueGenerale ()
	{
		uneVueGenerale.dispose();
	}
	public static void rendreVisibleVueConnexion (boolean action)
	{
		uneVueConnexion.setVisible(action);
	}
	
	public static Sondeur selectWhereUser(String email, String mdp) 
	{
		//avant d'aller � la base , on controle l'email et le mdp 
		
		Sondeur unUser = Modele.selectWhereUser(email, mdp); 
		
		return unUser; 
	}
	
	public static void main(String[] args) {
		 
		uneVueConnexion = new VueConnexion();
	
	}
}

