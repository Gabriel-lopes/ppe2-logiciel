package controleur;

public class Vote_sondage {
	private int idSondage, idSondeur, idQuestion, idReponse;
	private String question, nom, dateCreation, etat, reponse;
	
	public Vote_sondage(int idSondage, int idSondeur, int idQuestion, int idReponse, String question, String nom,
			String dateCreation, String etat, String reponse) 
	{
		this.idSondage = idSondage;
		this.idSondeur = idSondeur;
		this.idQuestion = idQuestion;
		this.idReponse = idReponse;
		this.question = question;
		this.nom = nom;
		this.dateCreation = dateCreation;
		this.etat = etat;
		this.reponse = reponse;
	}

	public int getIdSondage() {
		return idSondage;
	}

	public void setIdSondage(int idSondage) {
		this.idSondage = idSondage;
	}

	public int getIdSondeur() {
		return idSondeur;
	}

	public void setIdSondeur(int idSondeur) {
		this.idSondeur = idSondeur;
	}

	public int getIdQuestion() {
		return idQuestion;
	}

	public void setIdQuestion(int idQuestion) {
		this.idQuestion = idQuestion;
	}

	public int getIdReponse() {
		return idReponse;
	}

	public void setIdReponse(int idReponse) {
		this.idReponse = idReponse;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getDateCreation() {
		return dateCreation;
	}

	public void setDateCreation(String dateCreation) {
		this.dateCreation = dateCreation;
	}

	public String getEtat() {
		return etat;
	}

	public void setEtat(String etat) {
		this.etat = etat;
	}

	public String getReponse() {
		return reponse;
	}

	public void setReponse(String reponse) {
		this.reponse = reponse;
	}
	
	
}
